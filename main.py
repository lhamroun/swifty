#!/usr/bin/python3

import sys, os
import json
import requests
sys.path.insert(1, f"{os.path.abspath(os.getcwd())}/src/")
from view import *
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

DESKTOP = int(config('DESKTOP'))
if DESKTOP == 0:
    X, Y = (float(1080), float(1980))
else:
    X, Y = (float(config('X')), float(config('Y')))

if __name__ == '__main__':
    from kivy.core.window import Window
    Window.size = (X, Y)
    myApp = Swifty()
    myApp.run()


