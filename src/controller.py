#!/usr/bin/python3

import sys, os
import time
import re
import requests
import json
from decouple import config

import kivy
from kivy.app import App
from kivy.uix.image import Image, AsyncImage
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label

client_id = config('CLIENT_ID')
client_secret = config('CLIENT_SECRET')
DESKTOP = int(config('DESKTOP'))

if DESKTOP == 0:
    X, Y = (float(1080), float(1980))
else:
    X, Y = (float(config('X')), float(config('Y')))

def printRequests(r):
    if r is not None:
        print(f'requests status_code :{r.status_code}')
        if r.status_code == 200:
            print(f'----- ok      : {r.ok}')
            print(f'----- url     : {r.url}')
            print(f'----- request : {r.request}')
            print(f'----- text    : {json.dumps(r.text, indent=4)}')
            #print(f'----- text    : {r.text}')
        else:
            print(f'----- reason  : {r.reason}')
        print('-------------------------------')


def secureConnectToIntra():
    '''
        Take client_id and client_secret values to authentificate user with a
        generated token
            return:
        requests response (JSON format)
    '''
    try:
        r = requests.post('https://api.intra.42.fr/oauth/token',
            params={'grant_type':'client_credentials',
                'client_id': client_id,
                'client_secret': client_secret})
    except:
        print('you can\'t access to intra 42 because network fail')
        sys.exit(1)
        return None
    if r.status_code != 200:
        print('you can\'t access to intra 42')
        sys.exit(2)
        return None
    strToken = r.text
    return json.loads(strToken)


def getTokenInfo(token):
    r = requests.get('https://api.intra.42.fr/oauth/token/info', headers={'ContentType':'application/json', 'Authorization': 'Bearer ' + token['access_token']})
    print(f'Your token content is: {r.text}')


def getProjectIDs(project_name="", access_token=None, campus=1):
    r = requests.get(f"https://api.intra.42.fr/v2/projects", \
            headers={'Authorization': f"Bearer {access_token}"}, \
            params={'filter[name]': project_name})
    print(f"request url ---> {r.url}")
    # check requests return
    projectInfoJson = json.loads(r.text)
    result = [f['id'] for f in  projectInfoJson if campus in [g['id'] for g in f['campus']]]
    if len(result) == 0:
        return None
    return result


def getPresentUsers(campusId, access_token):
    ''' Take campus ID and return all users registered in cluster at this campus
    '''
    i = 1
    users = []
    tmpCount = -1
    userInfoJson = []
    stop = False
    while tmpCount != len(users):
        for x in range(3):
            r = requests.get(f"https://api.intra.42.fr/v2/campus/{campusId}/locations", \
                headers={'Authorization': f"Bearer {access_token}"}, \
                params={'page': str(i), 'per_page': 100, 'sort': ['-end_at'], 'filter[end_at]': ''})
            if r.status_code == 200:
                break
            print(f"request fail {x + 1}/3 because {r.reason}")
            time.sleep(0.3)
        if r.status_code != 200:
            i = i - 1
        userInfoJson = json.loads(r.text)
        userInfoJson = [f for f in userInfoJson if f['end_at'] is None]
        tmpCount = len(users)
        users += [f['user'] for f in userInfoJson]
        print(f"get users present at 42: page {i}")
        i += 1
    #if len(users) > 0:
    #    users = [f for f in users if re.search("^e[0-9]r\d+p\d+$", f['location'])]
    return users


def getUsersRegisterToProject(projectIDs, users, access_token):
    ''' Take project ID(s) and users list and check for each users if they did the project
    '''
    new_users = []
    for i, user in enumerate(users):
        for x in range(3):
            time.sleep(0.3)
            r = requests.get(f"https://api.intra.42.fr/v2/users/{user['id']}/projects_users",
                headers={'Authorization': f"Bearer {access_token}"})
            if r.status_code == 200:
                break
            print(f"    request fail {x + 1}/3 for {user['login']} because {r.reason}")
        if r.status_code != 200:
            print(f"ERROR to get {user['login']} infos because : {r.reason}")
            continue
        tmpUserJson = json.loads(r.text)
        tmpUserProjects = [f['project']['id'] for f in tmpUserJson]
        print(f"try to find if {user['login']}({i + 1}/{len(users)}) located on {user['location']} did project nb {projectIDs}")
        for f in tmpUserJson:
            if f['project']['id'] in projectIDs:
                print(f"        {user['login']} did or is register to the project")
                user['projects'] = f
                new_users.append(user)
    return new_users

def getProjectSuscriber(projectIDs, campusId, access_token):
    ''' Take project ID(s) and campus ID, and return all users who did the project
    '''
    users = []
    for projectID in projectIDs:
        tmpCount = -1
        i = 1
        while tmpCount != len(users):
            for x in range(3):
                print(f"get users registered to {projectID} : page {i}")
                r = requests.get(f"https://api.intra.42.fr/v2/projects/{projectID}/projects_users", \
                    headers={'Authorization': f"Bearer {access_token}"}, \
                    params={'page': str(i), 'per_page': 100, 'filter[campus]': campusId})
                if r.status_code == 200:
                    break
                print(f"request fail {x + 1}/3 because {r.reason}")
                time.sleep(0.3)
            if r.status_code != 200:
                i = i - 1 # ou continue pour skip la page de stud actuelle ?
            userInfoJson = json.loads(r.text)
            tmpCount = len(users)
            users += [f for f in userInfoJson]
            i = i + 1
            #print(f"nombre d'users inscrit a {projectID} = {len(users)}")
            #print(f"users are {[f['user']['login'] for f in users]}")
    if len(users) > 0:
        users = [f for f in users if f['user']['login'].find("3b3") == -1]
        for i, f in enumerate(users):
            users[i]['projects'] = {}
            users[i]['projects']['final_mark'] = f['final_mark']
    return users

def subRegisteredPresentUsers(registered_users, present_users):
    ''' filter present_users from registered_users and return an intervalle of the 2 lists
    '''
    users = []
    for registered_user in registered_users:
        for present_user in present_users:
            if registered_user['user']['login'] == present_user['login']:
                present_user.update(registered_user)
                users.append(present_user)
                break
        if len(users) > len(present_users):
            break
    return users

def getUserProjects(userID, access_token):
    ''' Take user ID and return all projects did by the user
    '''
    user_projects = []
    for x in range(5):
        r = requests.get(f"https://api.intra.42.fr/v2/users/{userID}/projects_users",
            headers={'Authorization': f"Bearer {access_token}"},
            params={'per_page': 100})
        if r.status_code == 200:
            break
        print(f"    request fail {x + 1}/5 for {userID} because {r.reason}")
        time.sleep(0.5)
    if r.status_code != 200:
        print(f"ERROR to get {userID} skills because : {r.reason}")
        return None
    user_projects = json.loads(r.text)
    user_projects = [{'name': f['project']['name'], 'final_mark': f['final_mark']} for f in user_projects if str(f['project']['name']).isdigit() is False]
    if len(user_projects) == 0:
        return None
    return {'projects': user_projects}

def getUserSkills(userID, access_token):
    ''' Take user ID and return all skills of the user
    '''
    for x in range(5):
        r = requests.get(f"https://api.intra.42.fr/v2/users/{userID}",
            headers={'Authorization': f"Bearer {access_token}"})
        if r.status_code == 200:
            break
        print(f"    request fail {x + 1}/5 for {userID} because {r.reason}")
        time.sleep(0.3)
    if r.status_code != 200:
        print(f"ERROR to get {userID} infos because : {r.reason}")
        return None
    userInfoJson = json.loads(r.text)
    if len(userInfoJson) == 0:
        return None
    user_skills = [f['level'] for f in userInfoJson['cursus_users']]
    user_skills = userInfoJson['cursus_users'][user_skills.index(max(user_skills))]
    return user_skills

def getUserByName(login, access_token):
    ''' Take a login and return user on json format
    '''
    for x in range(5):
        r = requests.get(f"https://api.intra.42.fr/v2/users",
            headers={'Authorization': f"Bearer {access_token}"},
            params={'filter[login]': login})
        if r.status_code == 200:
            break
        print(f"    request fail {x + 1}/5 for {login} because {r.reason}")
        time.sleep(0.3)
    if r.status_code != 200:
        print(f"ERROR to get {login} infos because : {r.reason}")
        return None
    user = json.loads(r.text)
    if len(user) == 0:
        return None
    user = user[0]
    return user

def getUsersByProject(project_name, access_token):
    ''' Take project name and return all users present at school who did the project
    '''
    method = 0
    paris = 1
    if len(project_name) == 0:
        return []
    projectIDs = getProjectIDs(project_name, access_token)
    print(f"project IDs of {project_name} ---> {projectIDs}\n")
    if projectIDs is None:
        return []
    present_users = getPresentUsers(paris, access_token)
    print(f"{len(present_users)} present at 42 Paris ---> {[f['login'] for f in present_users]}\n")
    if len(present_users) == 0:
        return []
    if len(present_users) < 15:
        method = 1
    if method == 0:
        ################### method 0: find all people who suscribe to project and check if present users are in
        registered_users = getProjectSuscriber(projectIDs, paris, access_token)
        print(f"{len(registered_users)} users inscit a {project_name} --> {[f['user']['login'] for f in registered_users]}")
        if len(registered_users) == 0:
            return []
        users = subRegisteredPresentUsers(registered_users, present_users)
        #print(f"{len(users)} users present at 42 Paris and registered to {project_name} ---> {users}\n")
        print(f"{len(users)} users present at 42 Paris and registered to {project_name} ---> {[f['login'] for f in users]}\n")
    else:
        ################### method 1: check if each present people did the project (can be slow if use this code for lot's of people)
        users = getUsersRegisterToProject(projectIDs, present_users, access_token)
        print(f"users registered or ever did project {projectIDs} ---> {[f['login'] for f in users]}\n")
    '''
    from datetime import datetime
    logFile = open(f"{datetime.now()}_{project_name}.log", "w")
    #logFile.write(str(users))
    logFile.write(json.dumps(users, indent=4))
    logFile.close()
    #sys.exit(0)
    '''
    return users

