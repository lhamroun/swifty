import sys, os

import kivy
from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.config import Config
from kivy.graphics import Rectangle, Color
import kivy.properties

from debug import getFakeUsers, getFakeUser
from controller import *

kivy.require('2.0.0')

DESKTOP = int(config('DESKTOP'))
if DESKTOP == 0:
    X, Y = (float(1080), float(1980))
else:
    X, Y = (float(config('X')), float(config('Y')))

def createNotFoundView(input_text, contentView):
    label = Label(text=f"No user or project called {input_text}")
    label.font_size = '25sp'
    if contentView is not None:
        contentView.size_hint = (1, None)
        contentView.height = Y - Y / 10
    return label

def createResultUserView(user, contentView):
    cell_size = 40
    text_size = 25
    contentView.size_hint = (1, None)
    contentView.height = Y - Y / 10
    contentView.size_hint = (1, 1)
    box = BoxLayout(orientation="vertical")
    userHeader = BoxLayout(orientation="horizontal")
    userHeader.size_hint = (1, 0.45)
    if user['photo'].find("3b3") != -1:
        photo = Image(source="assets/chat.jpg")
    else:
        photo = AsyncImage(source=user['photo'])
    print(f"createResultUserView -------------------------------> {photo}")
    subUserHeader = BoxLayout(orientation="vertical")
    login = Label(text=user['login'])
    login.font_size = X / 20
    level = Label(text=str(user['level']))
    level.font_size = X / 20
    userHeader.add_widget(photo)
    subUserHeader.add_widget(login)
    subUserHeader.add_widget(level)
    userHeader.add_widget(subUserHeader)

    location = Label(text=user['location'] if user['location'] is not None else "Not at 42")
    location.color = (1, 0, 0, 1) if user['location'] is None else (0, 1, 0, 1)
    location.size_hint = (1, 0.05)
    location.font_size = X / text_size#

    correction_point = Label(text=f"correction_point: {user['correction_point']}")
    correction_point.size_hint = (1, 0.5)
    correction_point.font_size = X / text_size

    tmp = ScrollView()
    tmp.padding_top = 30
    tmp.padding_bottom = 30
    project_box = BoxLayout(orientation="vertical")
    project_box.size_hint = (1, None)
    project_box.height = len(user['projects']) * cell_size
    for i, project in enumerate(user['projects']):
        tmp_project = BoxLayout(orientation="horizontal")
        tmp_project.size_hint = (1, None)
        tmp_project.height = cell_size
        tmp_project.padding_top = 10
        tmp_project.padding_bottom = 10
        tmp_project_name = Label(text=project['name'])
        tmp_project_name.font_size = X / (text_size + 10)
        if project['final_mark'] is None:
            mark = "registered"
            color = (0, 0, 1, 1)
        elif project['final_mark'] == 0:
            mark = "fail"
            color = (1, 0, 0, 1)
        else:
            mark = str(project['final_mark'])
            color = (0, 1, 0, 1)
        tmp_project_mark = Label(text=mark)
        tmp_project_mark.font_size = X / (text_size + 10)
        tmp_project_mark.color = color
        tmp_project.add_widget(tmp_project_name)
        tmp_project.add_widget(tmp_project_mark)
        project_box.add_widget(tmp_project)
    tmp.add_widget(project_box)

    tmp2 = ScrollView()
    skills_box = BoxLayout(orientation="vertical")
    skills_box.size_hint = (1, None)
    skills_box.height = len(user['skills']) * cell_size
    for i, skill in enumerate(user['skills']):
        tmp_skills = BoxLayout(orientation="horizontal")
        tmp_skills.size_hint = (1, None)
        tmp_skills.height = cell_size
        tmp_skills.padding_top = 10
        tmp_skills.padding_bottom = 10

        tmp_skills_name = Label(text=skill['name'])
        tmp_skills_name.font_size = X / (text_size + 10)
        tmp_skills_name.color = (0.9, 0.9, 0, 0.9)
        tmp_skills_mark = Label(text=str(round(skill['level'], 2)))
        tmp_skills_mark.font_size = X / (text_size + 10)
        tmp_skills_mark.size_hint = (None, 1)
        tmp_skills_mark.width = 150
        tmp_skills_mark.color = (1, 1, 0, 0.9)
        tmp_skills_percent = Label(text=f"{str(round(skill['level'] * 100 / 20, 2))}%")
        tmp_skills_percent.font_size = X / (text_size + 10)
        tmp_skills_percent.size_hint = (None, 1)
        tmp_skills_percent.width = 150
        tmp_skills_percent.color = (0.9, 0.9, 0, 0.9)

        tmp_skills.add_widget(tmp_skills_name)
        tmp_skills.add_widget(tmp_skills_mark)
        tmp_skills.add_widget(tmp_skills_percent)
        skills_box.add_widget(tmp_skills)
    tmp2.add_widget(skills_box)

    box.add_widget(userHeader)
    box.add_widget(location)
    box.add_widget(correction_point)
    box.add_widget(tmp)
    box.add_widget(tmp2)

    return box

def createResultScrollView(users, contentView):
    cell_size = Y / 10
    box = BoxLayout(orientation="vertical")
    contentView.size_hint = (1, None)
    contentView.height = len(users) * cell_size
    i = 0
    for user in users:
        user_box = BoxLayout(orientation="horizontal")
        user_box.size_hint = (1, None)
        user_box.height = cell_size
        user_box.padding_top = 20
        user_box.padding_bottom = 20
        with user_box.canvas:
            if user['final_mark'] == -1:
                Color(0, 0, 0.8, 0.3)
            elif user['final_mark'] == 0:
                Color(0.8, 0.5, 0, 0.3)
            else:
                Color(0, 0.8, 0, 0.3)
            user_box.rect = Rectangle(size=(contentView.width - 20, user_box.height - 20), pos=(10, (len(users) - i - 1) * cell_size + 10))
        tmpBox = BoxLayout(orientation="vertical", spacing=5)
        if user['photo'].find("3b3") != -1:
            photo = Image(source="assets/chat.jpg")
        else:
            photo = AsyncImage(source=user['photo'])
        login = Label(text=user['login'])
        tmpBox.add_widget(photo)
        tmpBox.add_widget(login)
        text_mark = str(user['final_mark'] if user['final_mark'] > -1 else "in progress")
        mark = Label(text=text_mark)
        location = Label(text=user['location'])
        user_box.add_widget(tmpBox)
        user_box.add_widget(mark)
        user_box.add_widget(location)
        box.add_widget(user_box)
        i = i + 1
    return box

def createHomeImageView(contentView):
    img = Image(source='assets/42logoBackground.png')
    if contentView is not None:
        contentView.size_hint = (1, None)
        contentView.height = Y - Y / 10
    return img


class AppView(BoxLayout):
    def __init__(self):
        super(AppView, self).__init__()
        self.users = []
        self.credentials = secureConnectToIntra()
        if self.credentials is None:
            print("network not reach")
        print(self.credentials)
        self.view = createHomeImageView(self.contentView)
        self.contentView.add_widget(self.view)
        #print(f"self ---> {dir(self)}")


    def goHomePage(self):
        print("HOME PAGE")
        self.contentView.remove_widget(self.view)
        self.view = createHomeImageView(self.contentView)
        self.contentView.add_widget(self.view)
        return True

    def findUserOrProject(self):
        print(f"try to find {self.searchBar.text}")
        result = self.findUsersByProject()
        if result is False:
            result = self.findUserByName()
            if result is False:
                self.view = createNotFoundView(self.searchBar.text, self.contentView)
                self.contentView.add_widget(self.view)
                return False
        self.searchBar.text = ""
        return True


    def findUsersByProject(self):
        if len(self.searchBar.text) == 0:
            return True
        self.contentView.remove_widget(self.view)
        if self.searchBar.text == "fake":
            self.users = getFakeUser()
        elif self.searchBar.text == "fakes":
            self.users = getFakeUsers()
        else:
            self.users = getUsersByProject(self.searchBar.text, self.credentials['access_token'])
        if len(self.users) == 0:
            return False
        self.users = [ {'login': f['login'],
                        'location': f['location'],
                        'photo': f['image']['versions']['small'],
                        'correction_point': f['correction_point'],
                        'final_mark': -1 if f['projects']['final_mark'] is None else f['projects']['final_mark']}
                        for f in self.users]
        print(f"users filtered ------------> {self.users}")
        self.view = createResultScrollView(self.users, self.contentView)
        self.contentView.add_widget(self.view)
        print(f"END OF RESEARCH")
        return True

    def findUserByName(self):
        if len(self.searchBar.text) == 0:
            return True
        self.contentView.remove_widget(self.view)
        self.user = getUserByName(self.searchBar.text, self.credentials['access_token'])
        if self.user is None:
            return False
        self.user = {'id': self.user['id'],
                    'login': self.user['login'],
                    'location': self.user['location'],
                    'photo': self.user['image']['versions']['small'],
                    'correction_point': self.user['correction_point']}
        skills = getUserSkills(self.user['id'], self.credentials['access_token']) # normalement getUserByName peut recuperer tout
        projects = getUserProjects(self.user['id'], self.credentials['access_token']) # normalement getUserByName peut recuperer tout
        #print(f"skills ------------> {skills}")
        #print(f"projects ------------> {projects}")
        if skills is not None:
            self.user.update(skills)
        if projects is not None:
            self.user.update(projects)
        print(f"user found ------------> {self.user}")
        self.view = createResultUserView(self.user, self.contentView)
        self.contentView.add_widget(self.view)
        self.searchBar.text = ""
        print(f"END OF RESEARCH")
        return True


class Swifty(App):

    def build(self):
        return AppView()
