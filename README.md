# Swifty

The aim of the project is to build an application that will retrieve the information of 42 students, using the 42 API. 
The project specifications is describe here : https://cdn.intra.42.fr/pdf/pdf/127191/en.subject.pdf

## Motivation

In 42 school, when you finish the common core, it's difficult to find other students to discuss about an advanced project.
I made this app in order to find a person present at the school who has already done or is working on the project search in the app.

## Installation

install python packages :
`python -m pip install -r requirements.txt`

install android-studio for mobile phone emulation
https://developer.android.com/studio



## How to setup API acess ?

You will need to create a `.env` file containing the following entries :

```
CLIENT_ID="c3fe48eb6b079d6b07175cc4f7f76372ec772bec9431fc3ed9bf18577eceb221"
CLIENT_SECRET="XXX"
X=480
Y=940
DESKTOP=1
```
**DESKTOP=0 for mobile version and 1 for desktop**\
**X=<pixel width>**\
**Y=<pixel height>**\
for mobile screen size is set to 1080x1980

